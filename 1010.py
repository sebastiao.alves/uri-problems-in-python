# Simple problem
# It uses python format function to define float number precision

c1, n1, p1 = input().split()
c2, n2, p2 = input().split()

valor = (int(n1)*float(p1) + int(n2)*float(p2))

print("VALOR A PAGAR: R$ {:.2f}".format(valor))
