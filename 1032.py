# Prime number algorithm
# Prime number list algorithm

from math import sqrt
primos = [2]
nxt=3
for i in range(3500):
    while True:
        eh_primo=True
        lim=sqrt(nxt)
        for pj in primos:
             if nxt%pj==0:
                 eh_primo=False
                 break
             if pj>lim:
                 break
        nxt+=1
        if eh_primo:
            primos.append(nxt-1)
            break
while True:
    n = int(input())
    if n==0:
        break
    s = [ i+1 for i in range(n)]
    i=0
    for k,pk in enumerate(primos[:n-1]):
        i=(i-1+pk)%len(s)
        s.pop(i)
    print(s[0])

