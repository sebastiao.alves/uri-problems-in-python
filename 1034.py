# Coin change problem
# Dynamic programming
from collections import defaultdict
casos=int(input())
for c in xrange(casos):
    n, m = [int(i) for i in raw_input().split()]
    a = sorted([int(i) for i in raw_input().split()])
    if n==1:
        print(m//a[0])
        continue
    maa=a[-1]
    r=defaultdict(lambda: m+1)
    if m%maa==0:
        print(m//maa)
        continue
    limite=m+1
    ma=a[-2]**2
    r[0]=0
    for i in xrange(1,limite):
        menor=limite
        for item in a[::-1]:
            if item>i:
                continue
            if 1+r[i-item]<menor:
                menor=1+r[i-item]
        r[i]=menor
        if menor!=limite and i>ma and (m-i)%maa==0:
            r[m]=(m-i)//maa+menor
            break

    print(r[m])

