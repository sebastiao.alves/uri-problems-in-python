# Simple problem

valor, centavos=[ int(v) for v in input().split('.')]
notas=[100,50,20,10,5,2]
moedas=[50,25,10,5,1]
cedulas=[]
pratas=[]
for n in notas:
    cedulas.append(valor//n)
    valor=valor%n
print("NOTAS:")
for i in range(len(notas)):
    print("{} nota(s) de R$ {}.00".format(cedulas[i],notas[i]))
for m in moedas:
    pratas.append(centavos//m)
    centavos=centavos%m
print("MOEDAS:")
print("{} moeda(s) de R$ {}.00".format(valor,1))
for i in range(len(moedas)):
    print("{} moeda(s) de R$ 0.{:02d}".format(pratas[i],moedas[i]))
