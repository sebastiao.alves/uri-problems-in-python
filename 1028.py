# Greatest Common Divisor

casos=int(input())
for c in range(casos):
    f1,f2=[int(x) for x in input().split()]
    while f2!=0:
        resto=f1%f2
        f1=f2
        f2=resto
    print(f1)
