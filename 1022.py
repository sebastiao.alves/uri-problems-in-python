# Simple problem
# Greatest Common Divisor algorithm

def mdc(a,b):
    while b!=0:
        tmp=b
        b=a%b
        a=tmp
    return a

casos=int(input())
for i in range(casos):
    entrada = input().split()
    N1 = int(entrada[0])
    D1 = int(entrada[2])
    op = entrada[3]
    N2 = int(entrada[4])
    D2 = int(entrada[6])
    if op == '+':
        N3 = N1*D2 + N2*D1
        D3 = D1*D2
    elif op == '-':
        N3 = N1*D2 - N2*D1
        D3 = D1*D2
    elif op == '*':
        D3 = D1*D2
        N3 = N1*N2
    else:
        N3 = N1*D2
        D3 = N2*D1
    divisor = mdc(N3,D3)
    N4 = N3 // divisor
    D4 = D3 // divisor
    print(N3,"/",D3," = ", N4,"/",D4,sep='')
