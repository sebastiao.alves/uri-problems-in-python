# Josephus problem
# Circular list

casos=int(input())
for c in range(casos):
    n,k = [int(i) for i in input().split()]
    s = [ i+1 for i in range(n)]
    i=0
    while len(s)!=1:
        i=(i-1+k)%len(s)
        s.pop(i)
    print("Case {}: {}".format(c+1, s[0]))

