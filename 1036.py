# Bhaskara formula
from math import sqrt

a, b, c = [float(x) for x in input().split()]

delta=b**2-4*a*c

if a==0 or delta<0:
    print("Impossivel calcular")
else:
    r1=(-b+sqrt(delta))/(2*a)
    r2=(-b-sqrt(delta))/(2*a)
    print("R1 = {:.5f}\nR2 = {:.5f}".format(r1,r2))

