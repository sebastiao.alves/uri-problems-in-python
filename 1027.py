# List compreension
# Dictionary

from sys import stdin
try: input = raw_input
except NameError: pass
while True:
    try:
        line = input()
    except EOFError:
        break
    num_pontos=int(line)
    if num_pontos==0:
        print("0")
        continue
    pts=[]
    for i in range(num_pontos):
        pts.append([int(i) for i in input().split()])
    pts.sort(key=lambda p: p[0])
    sB = {}
    sC = {}
    maior=-1
    pontos_y={} 
    for p in pts:
        x,y=p
        rp=repr(p)
        sB[rp]=1
        sC[rp]=1
        for p1 in pontos_y.get(y-2,[]):
            rp1=repr(p1)
            if x!=p1[0]:
                sC[rp]=sB[rp1]+1
                break
        for p1 in pontos_y.get(y+2,[]):
            if x!=p1[0]:
                rp1=repr(p1)
                sB[rp]=sC[rp1]+1
                break
        ly=pontos_y.setdefault(y,[])
        ly.insert(0,p)
    maior=max(max(sB.values()),max(sC.values()))
    print(maior)

