# List compreension, sort
# Binary search

def search(marbles,m):
    pos=-1
    start=0
    end=len(marbles)-1
    mid=(start+end)//2
    while end>=start:
       if marbles[start]==m:
           return start
       elif marbles[mid]==m:
           pos=mid

       if marbles[mid]>=m:
           end=mid-1
       else:
           start=mid+1
       mid=(start+end)//2
    return pos
       

n , q = [int(i) for i in input().split()]
caso=1
while n!=0 and q!=0:
    print("CASE# {}:".format(caso))
    marbles = []
    for i in range(n):
        marbles.append(int(input()))
    marbles.sort()
    for i in range(q):
        m=int(input())
        index=search(marbles,m)
        if index==-1:
            print(m, "not found")
        else:
            print(m, "found at", index+1)
    n , q = [int(i) for i in input().split()]
    caso +=1
