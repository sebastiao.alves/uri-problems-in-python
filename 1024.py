# List slice
# String functions (isalpha, ord, ...)

rounds = int(input())
for it in range(rounds):
    texto=input()
    texto2=''.join(chr(ord(char)+3) if char.isalpha() else char for char in texto)
    texto3 = texto2[::-1]
    texto4=texto3[:len(texto3)//2]+ ''.join(chr(ord(char)-1) for char in texto3[len(texto3)//2:])
    print(texto4)
