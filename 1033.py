# Fibonacci matrix O(log n) algorithm
# Fibonacci recursive calls count
# Matrix exponantiation algorithm

def mult_mat(mat1,mat2,b):
    a11,a12,a21,a22=mat1
    b11,b12,b21,b22=mat2
    return (a11*b11+a12*b21)%b, (a11*b12+a12*b22)%b, (a21*b11+a22*b21)%b, (a21*b12+a22*b22)%b

def pot_mat(mat,n,b):
    if n==1:
        return mat
    result=pot_mat(mat,n//2,b)
    result2=mult_mat(result,result,b)
    if n%2==1:
        result2=mult_mat(mat,result2,b)
    return result2

casos=1
while True:
    n,b = [int(i) for i in input().split()]
    if n==0 and b==0:
        break
    if n==0 or n==1:
        print('Case {}: {} {} 1'.format(casos,n,b))
        casos+=1
        continue
    original=1,1,1,0
    a11,a12,a21,a22 = pot_mat(original,n,b)
    c11,c12,c21,c22 = pot_mat(original,n-1,b)
    calls=(a11+a12+c12-1)%b
    print('Case {}: {} {} {}'.format(casos,n,b,calls))
    casos+=1

