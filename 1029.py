# Fibonacci sequential algorithm

casos=int(input())
c1=1
c2=1
r1=0
r2=1
chamadas=[1,1]
resultado=[0,1]
for i in range(2,40):
    r3=r2+r1
    c3=c2+c1+1
    chamadas.append(c3)
    resultado.append(r3)
    r1=r2
    r2=r3
    c1=c2
    c2=c3
    
for c in range(casos):
    N=int(input())
    print("fib({}) = {} calls = {}".format(N,chamadas[N]-1,resultado[N]))
