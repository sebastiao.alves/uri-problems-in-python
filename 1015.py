# Simple problem
# It uses python format function to define float number precision
# It uses sqrt math function

from math import sqrt
x1, y1 = [float(v) for v in input().split()]
x2, y2 = [float(v) for v in input().split()]
dist=sqrt((x2-x1)**2+(y2-y1)**2)
print("{:.4f}".format(dist))
